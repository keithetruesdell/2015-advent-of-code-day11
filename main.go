package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

const (
	ex1   string = "input/ex1.txt"
	input string = "input/input.txt"
)

var (
	answer string
)

func main() {

	ex := getFlags()
	f2r := file2Use(ex)
	data, err := readFile(f2r)
	if err != nil {
		fmt.Printf("ERROR with reading file\n     %v\n", err)
	}

	fmt.Println(data)

	fmt.Println("=========================")
	fmt.Printf("Answer is: %v\n", answer)
}

// readFile -
func readFile(f2r string) (data []string, err error) {

	inpF, err := os.Open(f2r)
	if err != nil {
		return data, err
	}

	defer inpF.Close()

	scn := *bufio.NewScanner(inpF)

	var tmpData []string
	for scn.Scan() {
		ln := strings.TrimSpace(scn.Text())
		tmpData = append(tmpData, ln)
	}

	for i := 0; i < len(tmpData[0]); i++ {
		data = append(data, string(tmpData[0][i]))
	}

	return data, err
}

// file2Use -
func file2Use(ex int) string {
	switch ex {
	case 0:
		return input
	case 1:
		return ex1
	default:
		return ex1
	}
}

// getFlags -
func getFlags() (ex int) {
	tmpEx := flag.Int("e", 1, "What is the example to use? 1 is the default, 0 is production")
	flag.Parse()
	ex = *tmpEx
	return ex
}
